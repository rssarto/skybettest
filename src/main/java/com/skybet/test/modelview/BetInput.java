package com.skybet.test.modelview;

import java.io.Serializable;

public class BetInput implements Serializable {
	
	private static final long serialVersionUID = -3478298253368740635L;
	private int bet_id;
	private double odds;
	private int stake;
	
	public BetInput(int bet_id, double odds, int stake) {
		this.bet_id = bet_id;
		this.odds = odds;
		this.stake = stake;
	}
	
	public BetInput(){}
	
	public int getBet_id() {
		return bet_id;
	}
	public void setBet_id(int bet_id) {
		this.bet_id = bet_id;
	}
	public double getOdds() {
		return odds;
	}
	public void setOdds(double odds) {
		this.odds = odds;
	}
	public int getStake() {
		return stake;
	}
	public void setStake(int stake) {
		this.stake = stake;
	}
	@Override
	public String toString() {
		return "BetInput [bet_id=" + bet_id + ", odds=" + odds + ", stake=" + stake + "]";
	}
}
