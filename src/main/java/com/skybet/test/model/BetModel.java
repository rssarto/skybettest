package com.skybet.test.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class BetModel implements Serializable {
	
	private static final long serialVersionUID = -6418092678938000357L;
	private int bet_id;
	private String event;
	private String name;
	private OddsModel odds;
	private int stake;
	private int transaction_id;
	public int getBet_id() {
		return bet_id;
	}
	public void setBet_id(int bet_id) {
		this.bet_id = bet_id;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public OddsModel getOdds() {
		return odds;
	}
	public void setOdds(OddsModel odds) {
		this.odds = odds;
	}
	public int getStake() {
		return stake;
	}
	public void setStake(int stake) {
		this.stake = stake;
	}
	public int getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}
	@Override
	public String toString() {
		return "BetView [bet_id=" + bet_id + ", event=" + event + ", name=" + name + ", odds=" + odds + "]";
	}
	
}
