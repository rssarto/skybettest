package com.skybet.test.modelview;

import java.math.BigDecimal;

public class BetView {

	private int bet_id;
	private String event;
	private String name;
	private OddsView odds;
	private int stake;
	private int transaction_id;
	
	public int getBet_id() {
		return bet_id;
	}
	public void setBet_id(int bet_id) {
		this.bet_id = bet_id;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getOdds() {
		return new BigDecimal(this.odds.getDenominator() + this.odds.getNumerator()).doubleValue();
	}
	public void setOdds(OddsView odds) {
		this.odds = odds;
	}
	public int getStake() {
		return stake;
	}
	public void setStake(int stake) {
		this.stake = stake;
	}
	public int getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}
}
