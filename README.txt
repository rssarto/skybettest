#####################################################
# STEPS TO BUILD AN RUN THE APPLICATION USING MAVEN #
#####################################################

1. In the command line console open the project folder

2. Excute the command below:
 
	mvn package && java -jar target/skybetTest-app-0.0.1-SNAPSHOT.jar

3. The REST methods in service can be called using the url http://localhost:8080/skybettest/