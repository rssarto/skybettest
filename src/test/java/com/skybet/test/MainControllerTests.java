package com.skybet.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.google.gson.Gson;
import com.skybet.test.controller.MainController;
import com.skybet.test.modelview.BetInput;

@RunWith(SpringRunner.class)
@WebMvcTest(MainController.class)
public class MainControllerTests {

	@Autowired
	private MockMvc mvc;
	
	@Test
	public void getAvaialble() throws Exception{
		this.mvc.perform(get("/available").accept(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
	}
	
	@Test
	public void betsWithSuccess() throws Exception{
		BetInput betInput = new BetInput(1,11.0,10);
		Gson gson = new Gson();
		String json = gson.toJson(betInput);
		this.mvc.perform(post("/bets").contentType(MediaType.APPLICATION_JSON_VALUE).content(json)).andExpect(status().isOk());
	}
	
	@Test
	public void betsWithInvalidData() throws Exception{
		BetInput betInput = new BetInput(1,9.0,7);
		Gson gson = new Gson();
		String json = gson.toJson(betInput);
		this.mvc.perform(post("/bets").contentType(MediaType.APPLICATION_JSON_VALUE).content(json)).andExpect(status().isIAmATeapot());
	}
	
	@Test
	public void betsWithUnsupportedMediaType() throws Exception{
		BetInput betInput = new BetInput(1,11.0,10);
		Gson gson = new Gson();
		String json = gson.toJson(betInput);
		this.mvc.perform(post("/bets").contentType(MediaType.TEXT_PLAIN).content(json)).andExpect(status().isUnsupportedMediaType());
	}
}
