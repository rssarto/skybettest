package com.skybet.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages={"com.skybet.test.controller","com.skybet.test.properties","com.skybet.test.mapper.util"})
public class SkybetTestAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkybetTestAppApplication.class, args);
	}
}
