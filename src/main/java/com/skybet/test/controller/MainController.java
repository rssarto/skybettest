package com.skybet.test.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.skybet.test.mapper.util.ListMapper;
import com.skybet.test.model.BetModel;
import com.skybet.test.model.OddsModel;
import com.skybet.test.modelview.BetInput;
import com.skybet.test.modelview.BetView;
import com.skybet.test.modelview.OddsView;
import com.skybet.test.properties.RestApiProperties;

@RestController
public class MainController {
	
	@Autowired
	private RestApiProperties restProperties;
	
	@Autowired
	private ListMapper listMapper;
	
    @Bean
    @ConfigurationProperties(prefix = "custom.rest.connection")
    public HttpComponentsClientHttpRequestFactory customHttpRequestFactory() 
    {
        return new HttpComponentsClientHttpRequestFactory();
    }	
	
	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate(customHttpRequestFactory());
	}

	@RequestMapping(value="/available", method=RequestMethod.GET)
	public ResponseEntity<List<BetView>> getAvaiable(RestTemplate restTemplate){
		ResponseEntity<List<BetView>> responseEntity = null;
		ResponseEntity<BetModel[]> extResponseEntity = restTemplate.getForEntity( restProperties.getUrl() + "/available", BetModel[].class);
		HttpStatus statusCode = extResponseEntity.getStatusCode();
		switch( statusCode ){
			case OK:{
				List<BetView> betViews = new ArrayList<>();
				Arrays.asList(extResponseEntity.getBody()).forEach( betModel -> { 
					BetView betView = listMapper.map(betModel, BetView.class);
					betView.setOdds(listMapper.map(betModel.getOdds(), OddsView.class));
					betViews.add(betView);
				} ); 
				responseEntity = new ResponseEntity<>(betViews, HttpStatus.OK);
				break;
			}
			default:{
				responseEntity = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		}
		return responseEntity;
	}
	
	@RequestMapping(value="/bets", method=RequestMethod.POST)
	public ResponseEntity<?> bets(@RequestBody BetInput betInput, RestTemplate restTemplate){
		ResponseEntity<?> responseEntity = null;
		BetModel betModel = new BetModel();
		betModel.setBet_id(betInput.getBet_id());
		betModel.setStake(betInput.getStake());
		betModel.setOdds(new OddsModel(betModel.getStake(), new BigDecimal(betInput.getOdds() - betModel.getStake()).intValue()));
		try{
			ResponseEntity<BetModel> extResponseEntity = restTemplate.postForEntity(restProperties.getUrl() + "/bets", betModel, BetModel.class, new HashMap<>());
			switch( extResponseEntity.getStatusCode() ){
				case CREATED:{
					BetView betView = listMapper.map(extResponseEntity.getBody(), BetView.class);
					betView.setOdds(listMapper.map(extResponseEntity.getBody().getOdds(), OddsView.class));
					responseEntity = new ResponseEntity<BetView>(betView, HttpStatus.OK);
					break;
				}
				default:{
					responseEntity = new ResponseEntity<String>("Unexpected failure",HttpStatus.INTERNAL_SERVER_ERROR);
					break;
				}
			}
		}catch(HttpClientErrorException ex){
			responseEntity = new ResponseEntity<String>(ex.getResponseBodyAsString(), HttpStatus.I_AM_A_TEAPOT);
		}
		return responseEntity;
	}
	
	
	 
}
