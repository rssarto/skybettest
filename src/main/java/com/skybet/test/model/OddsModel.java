package com.skybet.test.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class OddsModel implements Serializable {
	
	private static final long serialVersionUID = 8924914193404440371L;
	private int numerator;
	private int denominator;
	
	public OddsModel(int numerator, int denominator) {
		super();
		this.numerator = numerator;
		this.denominator = denominator;
	}
	
	public OddsModel(){}
	
	public int getNumerator() {
		return numerator;
	}
	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}
	public int getDenominator() {
		return denominator;
	}
	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}
	@Override
	public String toString() {
		return "OddsView [numerator=" + numerator + ", denominator=" + denominator + "]";
	}
}
